class katello_client::install (
    $katello_server = $katello_client::katello_server,
    $local_files = $katello_client::local_files
){

  if $::osfamily == 'Redhat' {

    # Remove Spacewalk / Sattelite packages
    exec {'Remove SW packages':
      path    => $::path,
      command => 'yum -y remove yum-rhn-plugin rhn-client-tools rhn-check rhn-setup rhnsd spacewalk-oscap > /var/tmp/sw_removed.sem',
      creates => '/var/tmp/sw_removed.sem'
    } ->

    # Create local files
    file { $local_files:
      ensure  => 'directory',
      source  => 'puppet:///modules/katello_client/packages',
      recurse => true,
    } ->

    exec {'Get CA Consumer':
      path    => $::path,
      command => "wget -O ${local_files}/katello-ca-consumer-latest.noarch.rpm http://${katello_server}/pub/katello-ca-consumer-latest.noarch.rpm",
      creates => "${local_files}/katello-ca-consumer-latest.noarch.rpm",
    }

    exec {'Install Dependencies':
      path    => $::path,
      command => "yum install --nogpgcheck ${local_files}/*.rpm -y > ${local_files}/installed.sem",
      creates => "${local_files}/installed.sem",
    }
  }
}

class katello_client::subscribe (
  $katello_server = $katello_client::katello_server,
  $katello_act_key = $katello_client::katello_act_key,
  $katello_org = $katello_client::katello_org
) {

  exec {'Subscribe':
    path      => $::path,
    logoutput => true,
    command   => "/usr/sbin/subscription-manager register --org=\"${katello_org}\" --activationkey=\"${katello_act_key}\" ",
    unless    => 'subscription-manager status | grep Current'
  }

  package {'katello-agent': ensure => 'installed', require => Exec['Subscribe'], }

  file {['/etc/yum.repos.d/CentOS-Base.repo','/etc/yum.repos.d/CentOS-CR.repo','/etc/yum.repos.d/CentOS-Debuginfo.repo','/etc/yum.repos.d/CentOS-fasttrack.repo','/etc/yum.repos.d/CentOS-Media.repo','/etc/yum.repos.d/CentOS-Sources.repo','/etc/yum.repos.d/CentOS-Vault.repo']:
    ensure => 'absent',
  }

}

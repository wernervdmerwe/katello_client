class katello_client (
  $katello_act_key,
  $katello_org,
  $katello_server = 'localhost',
  $katello_repo = 'https://fedorapeople.org/groups/katello/releases/yum/3.2/client/el7/x86_64/',
  $use_custom_epel = false,
  $custom_epel_repo = undef,
  $local_files = '/opt/katello-install'
) {

  if $::osfamily == 'Redhat' {
    include katello_client::install
    include katello_client::subscribe
  }
}
